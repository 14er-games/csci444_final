Names: Logan Cooper and Asa Farrer

Final Project

Rendering a quad of water in real time

Compiling: Use/edit the CMakeLists to compile and run the project.

Bugs: No known bugs

Other notes: This assignment we chose to use compute shaders for implementing parts of the shaders. As such, this requires an OpenGL 4.3 compatible system to run (and I'm pretty sure Macs don't work here)

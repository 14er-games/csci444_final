#version 430

uniform PermutationTable {
    int permutations[512];
};

uniform Time{
    float time;
};

//id var available to the noise functions
uint id_access = 0;

//Just using time alone resulted in extremely fast movement
float adjusted_time = -time / 2;

layout(local_size_x = 512) in;

layout(std430, binding=0) buffer Pos {
    vec4 data[];
};

uint getIndex(uint x, uint y) {
    return (256 * y) + x;
}

uint getX(uint index) {
    return index % 256;
}

uint getY(uint index) {
    return (index - getX(index)) / 256;
}

//Methods for generating noise
float fade(float t){
    return (6 * pow(t,5) - 15 * pow(t,4) + 10 * pow(t,3));
}

float grad(int hash, float x, float y) {
    int h = hash & 15;                      // CONVERT LO 4 BITS OF HASH CODE
    float u = h<8 ? x : y,                 // INTO GRADIENT DIRECTIONS.
    v = h<4 ? y : h==12||h==14 ? x : 0;
    return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

float noise(float x, float y){
    int X = int(floor(x)) & 255,                  // FIND UNIT CUBE THAT
    Y = int(floor(y)) & 255;                  // CONTAINS POINT.
    x -= floor(x);                                // FIND RELATIVE X,Y
    y -= floor(y);                                // OF POINT IN SQUARE.
    float u = fade(x),                                // COMPUTE FADE CURVES
    v = fade(y);                                // FOR EACH OF X,Y.
    int A = permutations[X  ]+Y, AA = permutations[A]  , AB = permutations[A+1]  ,      // HASH COORDINATES OF
    B = permutations[X+1]+Y, BA = permutations[B]  , BB = permutations[B+1]  ;      // THE SQUARE CORNERS,

    return mix(mix(grad(permutations[AA  ], x  , y  ), grad(permutations[BA  ], x-1, y  ), u),
    mix(grad(permutations[AB  ], x  , y-1), grad(permutations[BB  ], x-1, y-1), u), v);
}


float octavesPerlin(int octaves, float persistence){
    float total = 0.0;
    float freq = 0.06f;
    float amp = 0.3f;
    float maxVal = 0.0;
    for (int i = 0; i < octaves; i++){
//        total += noise(getX(id_access) * freq * cos((adjusted_time) + getX(id_access)) , getY(id_access) * freq * sin((adjusted_time) + getY(id_access)) ) * amp;
        total += noise(getX(id_access) * freq + adjusted_time, getY(id_access) * freq + adjusted_time) * amp;

        maxVal += amp;
        amp *= persistence;
        freq *= 2.0f;
    }
    return total/maxVal;
}


void main() {
    uint idx = gl_GlobalInvocationID.x;
    id_access = idx;

    //Increase amp for larger local waves
    float amp = 0.35;
    float t = (octavesPerlin(20,0.2) * amp);
//    t = sin(t);

    data[idx].w = t;
}

#version 430

uniform Time{
    float time;
};

layout(local_size_x = 512) in;

layout(std430, binding=0) buffer Pos {
    vec4 data[];
};

uint getIndex(uint x, uint y) {
    return (256 * y) + x;
}

uint getX(uint index) {
    return index % 256;
}

uint getY(uint index) {
    return (index - getX(index)) / 256;
}

//Basically height of waves
uniform float amplitude = 0.6;

//Tweak these values to manipluate waves
uniform float constantOffset = 0;
uniform float angularFreq = 1.2;
uniform int desiredWaves = 1;

//smaller values = bigger wavelength
//Likely want to increase amplitude if you increase this
uniform float invertedWaveLength = 0.008;

//Essentially determines the shapes of the waves
float phaseFunction(uint x, uint y){
    float float_x = float(x);
    float float_y = float(y);
    return ((x+y) * invertedWaveLength);
}


//Airy wave theory - from http://www.cse.chalmers.se/~uffe/xjobb/Gustav%20Olsson%20-%20Real-time%20water%20animation%20and%20rendering%20using%20wavefront%20parameter%20interpolation-small.pdf
//Modified for simplicity
float makeLinearWave(uint x, uint y){
    float waveHeight = 0.0;
    for (int i = 0; i < desiredWaves; i++){
        waveHeight += (amplitude * sin(angularFreq * phaseFunction(x,y) - angularFreq * time));
    }
    waveHeight += constantOffset;
    return waveHeight;
}

void main() {
    uint idx = gl_GlobalInvocationID.x;

    //comment both lines to see only perlin
    //Switch to this line to see only this shaders contribution
//    data[idx].w = makeLinearWave(getX(idx), getY(idx));
    data[idx].w += makeLinearWave(getX(idx), getY(idx));

}

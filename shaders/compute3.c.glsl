#version 430

layout(local_size_x = 512) in;

layout(std430, binding=0) buffer Pos {
    vec4 data[];
};

uint getIndex(uint x, uint y) {
    return (256 * y) + x;
}

uint getX(uint index) {
    return index % 256;
}

uint getY(uint index) {
    return (index - getX(index)) / 256;
}

vec3 getVec(uint x, uint y) {
    return vec3(x,  data[getIndex(x, y)].w, y);
}

vec3 getNormal(uint x1, uint y1, uint x2, uint y2, uint x3, uint y3) {
    if (x1 < 0 || y1 < 0 || x2 < 0 || y2 < 0 || x3 < 0 || y3 < 0 || x1 > 255 || y1 > 255 || x2 > 255 || y2 > 255 || x3 > 255 || y3 > 255) {
        return vec3(0, 1, 0);
    }

    vec3 vecOne = getVec(x2, y2) - getVec(x1, y1);
    vec3 vecTwo = getVec(x3, y3) - getVec(x1, y1);

    vec3 normal = cross(normalize(vecOne), normalize(vecTwo));

    if (normal.y < 0) {
        normal = -1 * normal;
    }

    return normal;
}

void main() {
    uint idx = gl_GlobalInvocationID.x;

    vec4 thisData = data[idx];
    uint x = getX(idx);
    uint y = getY(idx);

    vec3 normal = vec3(0, 0, 0);

    normal += getNormal(x, y, x, y+1, x+1, y);
    normal += getNormal(x, y, x+1, y, x, y-1);
    normal += getNormal(x, y, x, y-1, x-1, y);
    normal += getNormal(x, y, x-1, y, x, y+1);

    thisData.xyz = normalize(normal);
    data[idx] = thisData;
}

#version 430

in vec3 transformedVertexPos;
in vec3 normalVec;

out vec4 fragColorOut;

layout(location = 0) uniform samplerCube cubeMap;
uniform vec3 eyePos;

float distSquared (vec3 a, vec3 b) {
    vec3 c = a - b;
    return dot(c, c);
}

void main() {
    vec3 lightPos = vec3(-35, 2, -35);
    vec3 waterColor = vec3(0.109, 0.639, 0.925);

    vec3 lightDirection = normalize(lightPos - transformedVertexPos);

    vec3 normalViewingVector = normalize(eyePos - vec3(transformedVertexPos));
    vec3 halfwayVector = normalize(lightDirection + normalViewingVector);

    vec3 diffuseColor = waterColor * clamp(dot(lightDirection, normalVec), 0, 1);
    vec3 specularColor = waterColor * clamp(pow(max(dot(normalVec, halfwayVector), 0), 4), 0, 1);

    vec3 incidentRay = transformedVertexPos - eyePos;

    vec3 refrRay = refract(incidentRay, normalVec, 1.32);
    vec4 refrColor = texture(cubeMap, refrRay);

    vec3 reflRay = reflect(incidentRay, normalVec);
    vec4 reflColor = texture(cubeMap, reflRay);

    fragColorOut = vec4(diffuseColor + specularColor, 1);

    if (!gl_FrontFacing) {
        // We're under water so refract slightly more
        fragColorOut += 0.35 * refrColor;
        fragColorOut += 0.1 * reflColor;
        fragColorOut.a = 0.9;
    }
    else {
        // We're above water so reflect the environment slightly more
        fragColorOut += 0.1 * refrColor;
        fragColorOut += 0.35 * reflColor;
        fragColorOut.a = 0.8;
    }
}
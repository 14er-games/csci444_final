#version 430

layout(location = 0) in vec3 vertexPosition;

layout(std430, binding=0) buffer offsetBuffer {
    vec4 data[];
};

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

out vec3 transformedVertexPos;
out vec3 normalVec;

void main() {
    transformedVertexPos = vec3(modelMatrix * vec4(vertexPosition + vec3(0, data[gl_VertexID].w + (0.05 * gl_InstanceID), 0), 1));
    gl_Position = projectionMatrix * viewMatrix * vec4(transformedVertexPos, 1);
    mat3 normalMatrix = mat3(transpose(inverse(modelMatrix)));
    normalVec = normalize(normalMatrix * data[gl_VertexID].xyz);
}
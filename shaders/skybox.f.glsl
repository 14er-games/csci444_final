#version 430
// ***** FRAGMENT SHADER UNIFORMS *****
uniform samplerCube cubeMap;

// ***** FRAGMENT SHADER INPUT *****
in vec3 textureCoord;

// ***** FRAGMENT SHADER OUTPUT *****
out vec4 fragColorOut;

// ***** FRAGMENT SHADER SUBROUTINES *****


void main() {
//     fragColorOut = vec4( gl_FragCoord.xy/640.0f, 0.0f, 1.0f );
    fragColorOut = texture(cubeMap, textureCoord);
//    fragColorOut = vec4(textureCoord, 1);
    fragColorOut.a = 1;

    // if viewing the backside of the fragment,
    // reverse the colors as a visual cue
    if( !gl_FrontFacing ) {
        fragColorOut.rgb = fragColorOut.bgr;
    }
}
#version 430

layout(points) in;
layout(triangle_strip, max_vertices=4) out;

// ***** GEOMETRY SHADER UNIFORMS *****
uniform vec3 eyePosition;
uniform vec3 lookAtPoint;
uniform vec3 upVector;

// ***** GEOMETRY SHADER INPUT *****

// ***** GEOMETRY SHADER OUTPUT *****

out vec3 textureCoord;

// ***** GEOMETRY SHADER SUBROUTINES *****

void main() {

    vec3 z = normalize(eyePosition - lookAtPoint);
    vec3 x = normalize(cross(z, upVector));
    vec3 y = normalize(cross(x, z));

    gl_Position = vec4(-1.0f, 1.0f, -1.0f , 1.0f);
    textureCoord = -z + y - x;
    EmitVertex();

    gl_Position = vec4(-1.0f, -1.0f, -1.0f , 1.0f);
    textureCoord = -z - y - x;
    EmitVertex();

    gl_Position = vec4(1.0f, 1.0f, -1.0f , 1.0f);
    textureCoord = -z + y + x;
    EmitVertex();

    gl_Position = vec4(1.0f, -1.0f, -1.0f , 1.0f);
    textureCoord = -z - y + x;
    EmitVertex();

    EndPrimitive();
}
#version 430

// ***** VERTEX SHADER INPUT *****
layout(location=0) in vec3 vPos;     // vertex position

// ***** VERTEX SHADER OUTPUT *****

// ***** VERTEX SHADER SUBROUTINES *****

void main() {
    // pass our point through
    gl_Position = vec4( vPos, 1.0f );
}
